// Copyright Nat Weerawan 2015-2016
// MIT License

#include <Arduino.h>
#include <WiFiConnector.h>
#include <ESP8266WebServer.h>
// #include <SimpleWebRequestHandler.h>
#include <motor.h>
#include <sonar.h>

#ifndef WIFI_SSID
  #define WIFI_SSID       "WiFi-SSID"
  #define WIFI_PASSPHRASE "WIFI_PASSPHRASE"
#endif

ESP8266WebServer server(80);
WiFiConnector wifi(WIFI_SSID, WIFI_PASSPHRASE);
Motor left(4, 5, 16);
Motor right(12, 14, 13);
Sonar sonar(0, 15);
int dist = 100;
char buff[100];

int maxSpeed = PWMRANGE;
int halfSpeed = maxSpeed >> 1;

void init_hardware()
{
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  delay(10);
  Serial.println();
  Serial.println("BEGIN");
}

char * motorResponseJson(int left, int right) {
  sprintf(buff, "{\"left\" : %3d, \"right\" : %3d}\n", left, right);
  return buff;
}

// SimpleWebRequestHandler handler;

void init_webserver() {
  // server.addHandler(&handler);

  server.on ( "/", []() {
    bool state = digitalRead(LED_BUILTIN);
    digitalWrite(LED_BUILTIN, !state);
    String ms = String(millis());
    sprintf(buff, "{\"millis\": %s }", ms.c_str());
    server.send ( 200, "text/plain", buff );
  });

  server.on ( "/m", []() {
    String ms = String(millis());
    sprintf(buff, "{\"millis\": %s }", ms.c_str());
    server.send ( 200, "text/plain", buff );
  });

  server.on ("/f", [] () {
    server.send (200, "text/plain", motorResponseJson(maxSpeed, maxSpeed));
    left.forward(maxSpeed);
    right.forward(maxSpeed);
  });


  server.on ("/b", [] () {
    server.send (200, "text/plain", motorResponseJson(-maxSpeed, -maxSpeed));
    left.back(maxSpeed);
    right.back(maxSpeed);
  });

  server.on ("/l", [] () {
    server.send (200, "text/plain", motorResponseJson(-halfSpeed, halfSpeed));
    left.back(halfSpeed);
    right.forward(halfSpeed);
  });

  server.on ("/r", [] () {
    server.send (200, "text/plain", motorResponseJson(halfSpeed, -halfSpeed));
    left.forward(halfSpeed);
    right.back(halfSpeed);
  });

  server.on ("/bl", [] () {
    server.send (200, "text/plain", motorResponseJson(-halfSpeed, -maxSpeed));
    left.back(halfSpeed);
    right.back(maxSpeed);
  });

  server.on ("/br", [] () {
    server.send (200, "text/plain", motorResponseJson(-maxSpeed, -halfSpeed));
    left.back(maxSpeed);
    right.back(halfSpeed);
  });

  server.on ("/fl", [] () {
    server.send (200, "text/plain", motorResponseJson(halfSpeed, maxSpeed));
    left.forward(halfSpeed);
    right.forward(maxSpeed);
  });

  server.on ("/fr", [] () {
    server.send (200, "text/plain", motorResponseJson(maxSpeed, halfSpeed));
    left.forward(maxSpeed);
    right.forward(halfSpeed);
  });

  server.on("/s", [] () {
    server.send (200, "text/plain", motorResponseJson(0, 0));
    left.stop();
    right.stop();
  });

  server.on("/sn", [] () {
    char buf[100];
    sprintf(buf, "{ \"distance\" : %d }\n", dist);
    server.send(200, "text/plain", buf);
  });

}

void init_wifi() {
  wifi.init();
  wifi.on_connecting([&](const void* message)
  {
    Serial.print("Connecting to ");
    Serial.println(wifi.get("ssid") + ", " + wifi.get("password"));
    delay(200);
  });

  wifi.on_connected([&](const void* message)
  {
    // Print the IP address
    Serial.print("WIFI CONNECTED => ");
    Serial.println(WiFi.localIP());

    init_webserver();
    server.begin();
    Serial.println("SERVER Started.");
  });

  wifi.on_disconnected([&](const void* message) {
    server.close();
  });
}

void setup()
{
  init_hardware();
  init_wifi();
  wifi.connect();
}

void loop()
{
  dist = sonar.measureCM();
  wifi.loop();
  server.handleClient();
}
