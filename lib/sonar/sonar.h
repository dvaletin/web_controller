#ifndef _SONAR_H_
#define _SONAR_H_

#include <Arduino.h>

class Sonar {
  public:
    Sonar(uint8_t triggerPin, uint8_t echoPin);
    int measureCM();

  private:
    uint8_t _trigger_pin;
    uint8_t _echo_pin;
};


#endif
