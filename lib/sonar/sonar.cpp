#include <Arduino.h>
#include "sonar.h"

#define US_CM_DURATION 58
#define TRIGGER_PIN _pin_1
#define ECHO_PIN _pin_2


Sonar::Sonar(uint8_t triggerPin, uint8_t echoPin) {
  _trigger_pin = triggerPin;
  _echo_pin = echoPin;
  pinMode(_trigger_pin, OUTPUT);
  pinMode(_echo_pin, INPUT);
}

int Sonar::measureCM() {
  int duration;
  digitalWrite(_trigger_pin, LOW);
  delayMicroseconds(2);
  digitalWrite(_trigger_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(_trigger_pin, LOW);
  duration = pulseIn(_echo_pin, HIGH);
  return duration / US_CM_DURATION;
}
