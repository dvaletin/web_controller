#include "Arduino.h"
#include "motor.h"

Motor::Motor(uint8_t pin1, uint8_t pin2, uint8_t ana1) {
  _pin_1 = pin1;
  _pin_2 = pin2;
  _ana_1 = ana1;
  pinMode(_pin_1, OUTPUT);
  pinMode(_pin_2, OUTPUT);
  pinMode(_ana_1, OUTPUT);
}


void Motor::stop() {
  digitalWrite(_pin_1, LOW);
  digitalWrite(_pin_2, LOW);
  analogWrite(_ana_1, 0);
}

void Motor::forward(uint16_t speed) {
  digitalWrite(_pin_1, HIGH);
  digitalWrite(_pin_2, LOW);
  analogWrite(_ana_1, speed);
}

void Motor::back(uint16_t speed) {
  digitalWrite(_pin_1, LOW);
  digitalWrite(_pin_2, HIGH);
  analogWrite(_ana_1, speed);
}

void Motor::setSpeed(uint16_t speed) {
  analogWrite(_ana_1, speed);
}

uint8_t Motor::getDirection() {
  return 0;
}
