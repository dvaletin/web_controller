#ifndef _MOTOR_H_
#define _MOTOR_H_


class Motor {
  public:
    /**

    */
    Motor(uint8_t pin1, uint8_t pin2, uint8_t ana1);
    void forward(uint16_t speed);
    void back(uint16_t speed);
    void setSpeed(uint16_t speed);
    void stop();
    uint8_t getDirection();

  private:
    uint8_t _pin_1;
    uint8_t _pin_2;
    uint8_t _ana_1;
};


#endif
