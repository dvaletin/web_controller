# README #

NodeMCU esp8266 web controller for robot. Has endpoints to control robot movement {forward, forward-left, left, back-left, back, back-right, right, forward-right} and sonar distance measurment endpoint.

### What is this repository for? ###

* esp8266 based web controller for managing two-wheels robot
* 1.0

### How do I get set up? ###

* Install PlatformIO IDE (http://platformio.org/)
* Open project in IDE
* Change WIFI_SSID and WIFI_PASSPHRASE in main.cpp
* Adjust maxSpeed and halfSpeed values if needed
* Compile and upload to your esp8266
* Connect terminal at 115200 baud and wait for IP address message
* Use CURL or Android app () to control your robot:
  curl http://<IP>/<COMMAND>
  where COMMAND is:
  * s for stop
  * f for forward
  * l for left
  * r for right
  * b for back
  * fl for forward-left
  * fr for forward-right
  * bl for back-left
  * br for back-right

### Who do I talk to? ###

* Repo owner - https://www.facebook.com/dmitry.valetin
